if has('nvim')
  set t_8f=38;2;%lu;%lu;%lum
  set t_8b=48;2;%lu;%lu;%lum
  set termguicolors
endif
colorscheme cwcolors
syntax enable

if $REPOBASE != ""
	set tags=$REPOBASE/.git/tags
endif

let c_space_errors=1
