local use = require('packer').use

require('packer').startup(function()
	use 'wbthomason/packer.nvim' -- Package manager
	use 'neovim/nvim-lspconfig' -- Collection of configurations for the built-in LSP client

	use 'scrooloose/nerdtree'
	use 'PhilRunninger/nerdtree-visual-selection'
	use 'Xuyuanp/nerdtree-git-plugin'
	--	:45 has to have -nargs= set to sth different than 0 (pull request https://github.com/scrooloose/nerdtree-project-plugin/pull/2/files says ?)
	--	Changed to 1, as the preceeding lines have it set to 1 as well
	use 'scrooloose/nerdtree-project-plugin'
	use 'PhilRunninger/nerdtree-buffer-ops'
	use 'tiagofumo/vim-nerdtree-syntax-highlight'

	use 'ap/vim-css-color'

	-- use 'vim-syntastic/syntastic'
	use 'tpope/vim-surround'
	use 'jmcomets/vim-pony'

	use 'ycm-core/YouCompleteMe'
	use 'tweekmonster/django-plus.vim'
	--use 'SirVer/ultisnips'
	-- use 'scrooloose/nerdtree-project-plugin'

	-- use 'jupyter-vim/jupyter-vim'
	use 'goerz/jupytext.vim'
end)

vim.cmd([[
	augroup packer_user_config
		autocmd!
		autocmd BufWritePost plugins.lua source <afile> | PackerSync
	augroup end
]])
