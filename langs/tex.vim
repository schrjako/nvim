nnoremap <C-B> :!make<CR>

set smartindent
set foldmethod=indent

inoremap <M-c> \v<space>c
inoremap <M-z> \v<space>z
inoremap <M-s> \v<space>s

inoremap <M-C> \v<space>C
inoremap <M-Z> \v<space>Z
inoremap <M-S> \v<space>S

set textwidth=80
set syntax=tex
