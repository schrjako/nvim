set rtp^="/home/jakob/.opam/default/share/ocp-indent/vim"
set tw=0

set foldmethod=syntax
nnoremap <C-B> :!make<CR>
