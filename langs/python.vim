set sw=4
set ts=4
set sts=4

set foldmethod=indent

autocmd BufWritePre *.py :%s/\s\+$//e
