inoremap <M-c> č
inoremap <M-z> ž
inoremap <M-s> š
inoremap <M-d> đ

inoremap <M-C> Č
inoremap <M-Z> Ž
inoremap <M-S> Š
inoremap <M-D> Đ
