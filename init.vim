for rcfile in split(globpath("$HOME/.config/nvim/rc", "*.vim"), '\n')
	execute('source '.rcfile)
endfor

lua require('init')

set fileencodings=utf-8
set fileencoding=utf-8
set encoding=utf-8
set mouse=

set rnu!
set nu
set tabstop=2
set shiftwidth=2
set linebreak
set tabpagemax=500
set scrolloff=4

set foldlevel=1000

set tags=./tags,.git/tags,../.git/tags,../../.git/tags
"set hlsearch
"set colorcolumn=80
syntax on
filetype plugin on
filetype indent on
set nocp

"au VimEnter * if &diff | execute 'windo set wrap' | endif
au VimEnter * execute 'windo set wrap'

no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>
ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>

noremap <C-P> <esc>:FZF<CR>

augroup filetypes
	autocmd!
	autocmd FileType python source $HOME/.config/nvim/langs/python.vim
	autocmd FileType htmldjango source $HOME/.config/nvim/langs/htmldjango.vim
	autocmd FileType ocaml source $HOME/.config/nvim/langs/ocaml.vim
	autocmd FileType markdown source $HOME/.config/nvim/langs/md.vim
	autocmd FileType html source $HOME/.config/nvim/langs/html.vim
	autocmd FileType c source $HOME/.config/nvim/langs/c.vim
	autocmd FileType cpp source $HOME/.config/nvim/langs/cpp.vim
	autocmd FileType tex source $HOME/.config/nvim/langs/tex.vim
	autocmd FileType plaintex source $HOME/.config/nvim/langs/tex.vim
	autocmd FileType php source $HOME/.config/nvim/langs/php.vim
	autocmd BufRead *.ipynb source $HOME/.config/nvim/langs/jupyter_notebook.vim
augroup END

nnoremap <C-B> :!make<CR>
